/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversionmonedas;

import java.util.Scanner;

/**
 *
 * @author Alejandro Soler Sanchis (1 DAW)
 */
public class ConversionMonedas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int tipoMoneda;
        //pedimos la moneda que quiere cambiar
        do {
            System.out.println("Que moneda vas  a quires hacer el cambio?(Introduce el numero de la moneda)");
            System.out.println("1--Euros \n2--Libras \n3--Dolar \n4--Yen \n5--Rupias ");
            tipoMoneda = sc.nextInt();
        } while (tipoMoneda < 1 || tipoMoneda > 5);
        int cambioMoneda;
        double cantidad;
        //pedimos la moneda a relaizar el cambio
        do {
            System.out.println("Introduce la cantidad");
            cantidad = sc.nextDouble();
            System.out.println("A que moneda vas  a queres hacer el cambio?(Introduce el numero de la moneda)");
            System.out.println("1--Euros \n2--Libras \n3--Dolar \n4--Yen \n5--Rupias ");
            cambioMoneda = sc.nextInt();
        } while (cambioMoneda < 1 || cambioMoneda > 5);
        Cambio(tipoMoneda, cambioMoneda, cantidad);

    }

    /**
     * metodo que recibe los tipos de monedas y la cantidad a cambiar y segun los parametros establecidos se realiza el cambio
     *
     * @param tipoMoneda recibe la moneda que quiere cambiar
     * @param cambioMonedarecibe la moneda a cambiar
     * @param cantidad recibe la cantidad
     */
    public static void Cambio(int tipoMoneda, int cambioMoneda, double cantidad) {
        switch (tipoMoneda) {
            case 1:
                switch (cambioMoneda) {
                    case 1:
                        System.out.println("Has introducido la misma moneda");
                        break;
                    case 2:
                        cantidad = cantidad * 0.85;
                        System.out.println(cantidad + " libras");
                        break;
                    case 3:
                        cantidad = cantidad * 1.10;
                        System.out.println(cantidad + " dolares");
                        break;
                    case 4:
                        cantidad = cantidad * 120.78;
                        System.out.println(cantidad + " yenes");
                        break;
                    case 5:
                        cantidad = cantidad * 74.34;
                        System.out.println(cantidad + " rupias");
                        break;

                }
                break;
            case 2:
                switch (cambioMoneda) {
                    case 1:
                        cantidad = cantidad * 1.18;
                        System.out.println(cantidad + " euros");
                        break;
                    case 2:
                        System.out.println("Has introducido la misma moneda");
                        break;
                    case 3:
                        cantidad = cantidad * 1.30;
                        System.out.println(cantidad + " dolares");
                        break;
                    case 4:
                        cantidad = cantidad * 142.72;
                        System.out.println(cantidad + " yenes");
                        break;
                    case 5:
                        cantidad = cantidad * 92.55;
                        System.out.println(cantidad + " rupias");
                        break;

                }
                break;
            case 3:
                switch (cambioMoneda) {
                    case 1:
                        cantidad = cantidad * 0.91;
                        System.out.println(cantidad + " euros");
                        break;
                    case 2:
                        cantidad = cantidad * 0.77;
                        System.out.println(cantidad + " libras");
                        break;
                    case 3:
                        System.out.println("Has introducido la misma moneda");
                        break;
                    case 4:
                        cantidad = cantidad * 109.75;
                        System.out.println(cantidad + " yenes");
                        break;
                    case 5:
                        cantidad = cantidad * 71.20;
                        System.out.println(cantidad + " rupias");
                        break;

                }
                break;
            case 4:
                switch (cambioMoneda) {
                    case 1:
                        cantidad = cantidad * 0.0083;
                        System.out.println(cantidad + " euros");
                        break;
                    case 2:
                        cantidad = cantidad * 0.070;
                        System.out.println(cantidad + " libras");
                        break;
                    case 3:
                        cantidad = cantidad * 0.0091;
                        System.out.println(cantidad + " dolares");
                        break;
                    case 4:
                        System.out.println("Has introducido la misma moneda");
                        break;
                    case 5:
                        cantidad = cantidad * 125.36;
                        System.out.println(cantidad + " rupias");
                        break;

                }
                break;
            case 5:
                switch (cambioMoneda) {
                    case 1:
                        cantidad = cantidad * 0.013;
                        System.out.println(cantidad + " euros");
                        break;
                    case 2:
                        cantidad = cantidad * 0.011;
                        System.out.println(cantidad + " libras");
                        break;
                    case 3:
                        cantidad = cantidad * 0.014;
                        System.out.println(cantidad + " dolares");
                        break;
                    case 4:
                        cantidad = cantidad * 1.54;
                        System.out.println(cantidad + " yenes");
                        break;
                    case 5:
                        System.out.println("Has introducido la misma moneda");
                        break;

                }

        }

    }
}
