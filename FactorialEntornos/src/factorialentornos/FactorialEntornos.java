/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorialentornos;

import java.util.Scanner;

/**
 *
 * @author Izán José Chumillas Rodríguez (1 DAW)
 * @version 1.0
 * @since 05.02.20
 */
public class FactorialEntornos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Introduce un número");
        int num = teclado.nextInt();
        int i;
        int factorial = 1;
        for (i = 2; i <= num; i++) {
            factorial = factorial * i;

        }
        System.out.println(factorial);
    }

}
