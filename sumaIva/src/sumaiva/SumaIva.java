/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sumaiva;

import java.util.Scanner;

/**
 *
 * @author Alberto Ortega (1 DAW)
 * @version 1.0
 * @since 05.02.20
 */
public class SumaIva {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //cantidad de valores a introducir
        System.out.println("¿Cuántos productos vas a sumar?");
        int valores = sc.nextInt();
        double importe =  0;
        //bucle para sumar el valor de los productos
        for (int i = 0; i < valores; i++) {
            System.out.println("Introduce el valor del producto");
            importe += sc.nextDouble();
        }
        //mosntramos el resultado del importe
        System.out.println("El valor de los productos es " +(importe*1.21));
    }
    
}
