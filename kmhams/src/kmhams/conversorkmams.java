/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kmhams;

import java.util.Scanner;

/**
 *programa kmams
 * @author Enrique García Alcañiz (1 DAW)
 */
public class conversorkmams {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int decision;
        do{
        System.out.println("Quieres pasar de km/h a m/s (1) o de m/s a km/h (2)");
        decision = sc.nextInt();
        }while(decision>3 || decision<0);
        double km;
        double ms;
        if (decision == 1) {
            System.out.println("Dame un valor en km/h");
            km = sc.nextDouble();
            System.out.println("Ahora lo pasaré a m/s");
            ms = km / 3.6;
            System.out.println(km + "Km/h son " + ms + " m/s");
        } else {
            System.out.println("Dame un valor en m/s");
            ms = sc.nextDouble();
            System.out.println("Ahora lo pasaré a km/h");
            km = ms*3.6;
            System.out.println(ms+ "m/s son "+km+" km/h");
        }

    }

}